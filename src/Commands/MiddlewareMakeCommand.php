<?php

namespace IEfremov\ModuleDesigner\Commands;

use Illuminate\Routing\Console\MiddlewareMakeCommand as BaseMiddlewareMakeCommand;
use Symfony\Component\Console\Attribute\AsCommand;
use IEfremov\ModuleDesigner\Traits\Command;

#[AsCommand(name: 'efremov:make_middleware')]
class MiddlewareMakeCommand extends BaseMiddlewareMakeCommand
{
    use Command;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'efremov:make_middleware';
}