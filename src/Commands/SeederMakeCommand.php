<?php

namespace IEfremov\ModuleDesigner\Commands;

use Illuminate\Database\Console\Seeds\SeederMakeCommand as BaseSeederMakeCommand;
use Symfony\Component\Console\Attribute\AsCommand;
use IEfremov\ModuleDesigner\Traits\Command;
use Illuminate\Support\Str;

#[AsCommand(name: 'efremov:make_seeder')]
class SeederMakeCommand extends BaseSeederMakeCommand
{
    use Command;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'efremov:make_seeder';

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = str_replace('\\', '/', Str::replaceFirst($this->rootNamespace(), '', $name));

        $rootPath = config('module_config.path');

        return base_path() . 
            '/' . $rootPath . '/' . 
            str_replace('\\', '/', $this->rootNamespace()) . $name . '.php';
    }

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace()
    {
        return config('module_config.namespace') . 
            '\\' . config('module_config.name') . 
            '\\' . 'Database\Seeders\\';
    }
}