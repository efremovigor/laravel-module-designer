<?php

namespace IEfremov\ModuleDesigner\Commands;

use Illuminate\Database\Console\Factories\FactoryMakeCommand as BaseFactoryMakeCommand;
use Symfony\Component\Console\Attribute\AsCommand;
use IEfremov\ModuleDesigner\Traits\Command;
use Illuminate\Support\Str;

#[AsCommand(name: 'efremov:make_factory')]
class FactoryMakeCommand extends BaseFactoryMakeCommand
{
    use Command;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'efremov:make_factory';

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $factory = class_basename(Str::ucfirst(str_replace('Factory', '', $name)));

        /** @var string $optionModel */
        $optionModel = $this->option('model');
        $namespaceModel = $optionModel
                        ? $this->qualifyModel($optionModel)
                        : $this->qualifyModel($this->guessModelName($name));

        $model = class_basename($namespaceModel);

        $namespace = $this->getNamespace(
            Str::replaceFirst(
                $this->rootNamespace(), 
                $this->rootNamespace() . 'Database\\Factories\\', 
                $this->qualifyClass($this->getNameInput())
            )
        );
        
        $replace = [
            '{{ factoryNamespace }}' => $namespace,
            'NamespacedDummyModel' => $namespaceModel,
            '{{ namespacedModel }}' => $namespaceModel,
            '{{namespacedModel}}' => $namespaceModel,
            'DummyModel' => $model,
            '{{ model }}' => $model,
            '{{model}}' => $model,
            '{{ factory }}' => $factory,
            '{{factory}}' => $factory,
        ];

        $stub = $this->files->get($this->getStub());

        $t = $this->replaceNamespace($stub, $name)->replaceClass($stub, $name);

        return str_replace(
            array_keys($replace), array_values($replace), $t
        );
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = (string) Str::of($name)->replaceFirst($this->rootNamespace(), '')->finish('Factory');
        $rootPath = config('module_config.path');

        return base_path() .
            '/' . $rootPath . '/' .
            str_replace('\\', '/', $this->rootNamespace()) .
            '/Database/Factories/'.str_replace('\\', '/', $name).'.php';
    }

}