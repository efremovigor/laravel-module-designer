<?php

namespace IEfremov\ModuleDesigner\Commands;

use Illuminate\Foundation\Console\ModelMakeCommand as BaseModelMakeCommand;
use Symfony\Component\Console\Attribute\AsCommand;
use IEfremov\ModuleDesigner\Traits\Command;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;

#[AsCommand(name: 'efremov:make_model')]
class ModelMakeCommand extends BaseModelMakeCommand
{
    use Command;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'efremov:make_model';

    /**
     * Create a model factory for the model.
     *
     * @return void
     */
    protected function createFactory()
    {
        /** @var string $name */
        $name = $this->argument('name');
        $factory = Str::studly($name);

        $this->call('efremov:make_factory', [
            'name' => "{$factory}Factory",
            '--model' => $this->qualifyClass($this->getNameInput()),
        ]);
    }

    /**
     * Create a migration file for the model.
     *
     * @return void
     */
    protected function createMigration()
    {
        /** @var string $name */
        $name = $this->argument('name');
        $table = Str::snake(Str::pluralStudly(class_basename($name)));

        if ($this->option('pivot')) {
            $table = Str::singular($table);
        }

        $this->call('efremov:make_migration', [
            'name' => "create_{$table}_table",
            '--create' => $table,
        ]);
    }

    /**
     * Create a seeder file for the model.
     *
     * @return void
     */
    protected function createSeeder()
    {
        /** @var string $name */
        $name = $this->argument('name');
        $seeder = Str::studly(class_basename($name));

        $this->call('efremov:make_seeder', [
            'name' => "{$seeder}Seeder",
        ]);
    }

        /**
     * Create a controller for the model.
     *
     * @return void
     */
    protected function createController()
    {
        /** @var string $name */
        $name = $this->argument('name');
        $controller = Str::studly(class_basename($name));

        $modelName = $this->qualifyClass($this->getNameInput());

        $this->call('efremov:make_controller', array_filter([
            'name' => "{$controller}Controller",
            '--model' => $this->option('resource') || $this->option('api') ? $modelName : null,
            '--api' => $this->option('api'),
            '--requests' => $this->option('requests') || $this->option('all'),
            '--test' => $this->option('test'),
            '--pest' => $this->option('pest'),
        ]));
    }

    /**
     * Create a policy file for the model.
     *
     * @return void
     */
    protected function createPolicy()
    {
        /** @var string $name */
        $name = $this->argument('name');
        $policy = Str::studly(class_basename($name));

        $this->call('efremov:make_policy', [
            'name' => "{$policy}Policy",
            '--model' => $this->qualifyClass($this->getNameInput()),
        ]);
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\\Models';
    }
}