<?php

namespace IEfremov\ModuleDesigner\Commands;

use Illuminate\Foundation\Console\PolicyMakeCommand as BasePolicyMakeCommand;
use Symfony\Component\Console\Attribute\AsCommand;
use IEfremov\ModuleDesigner\Traits\Command;

#[AsCommand(name: 'efremov:make_policy')]
class PolicyMakeCommand extends BasePolicyMakeCommand
{
    use Command;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'efremov:make_policy';
}