<?php

namespace IEfremov\ModuleDesigner\Commands;

use Illuminate\Database\Console\Migrations\MigrateMakeCommand as BaseMigrationMakeCommand;
use IEfremov\ModuleDesigner\Traits\Command;
use Illuminate\Database\Migrations\MigrationCreator;
use Illuminate\Support\Composer;

class MigrationMakeCommand extends BaseMigrationMakeCommand
{
    use Command;

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'efremov:make_migration {name : The name of the migration}
        {--create= : The table to be created}
        {--table= : The table to migrate}
        {--path= : The location where the migration file should be created}
        {--realpath : Indicate any provided migration file paths are pre-resolved absolute paths}
        {--fullpath : Output the full path of the migration (Deprecated)}';

    /**
     * Get the path to the migration directory.
     *
     * @return string
     */
    protected function getMigrationPath()
    {
        $rootPath = config('module_config.path');

        return base_path() . 
            '/' . $rootPath . '/' . 
            str_replace('\\', '/', $this->rootNamespace()) . '/Database/Migrations';
    }
}