<?php

namespace IEfremov\ModuleDesigner\Commands;

use Illuminate\Foundation\Console\RequestMakeCommand as BaseRequestMakeCommand;
use Symfony\Component\Console\Attribute\AsCommand;
use IEfremov\ModuleDesigner\Traits\Command;

#[AsCommand(name: 'efremov:make_request')]
class RequestMakeCommand extends BaseRequestMakeCommand
{
    use Command;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'efremov:make_request';
}