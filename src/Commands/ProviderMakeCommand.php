<?php

namespace IEfremov\ModuleDesigner\Commands;

use Illuminate\Foundation\Console\ProviderMakeCommand as BaseProviderMakeCommand;
use Symfony\Component\Console\Attribute\AsCommand;
use IEfremov\ModuleDesigner\Traits\Command;

#[AsCommand(name: 'efremov:make_provider')]
class ProviderMakeCommand extends BaseProviderMakeCommand
{
    use Command;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'efremov:make_provider';
}