<?php

use Illuminate\Support\Facades\Route;
use IEfremov\ModuleDesigner\Http\Controllers\ModuleDesignerController;

Route::get('/module-designer/create', [ModuleDesignerController::class, 'create'])->name('module-designer.create');
Route::post('/module-designer/process', [ModuleDesignerController::class, 'process'])->name('module-designer.process');