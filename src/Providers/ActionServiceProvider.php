<?php

namespace IEfremov\ModuleDesigner\Providers;

use Illuminate\Support\ServiceProvider;
use IEfremov\ModuleDesigner\Contracts\Actions\GetModuleComponentsOptions;
use IEfremov\ModuleDesigner\Contracts\Actions\CreateModuleRequestRule;
use IEfremov\ModuleDesigner\Contracts\Actions\FilterCreateRequest;
use IEfremov\ModuleDesigner\Contracts\Actions\CallModuleComponentCommand;
use IEfremov\ModuleDesigner\Actions\GetModuleComponentsOptionsAction;
use IEfremov\ModuleDesigner\Actions\CreateModuleRequestRuleAction;
use IEfremov\ModuleDesigner\Actions\FilterCreateRequestAction;
use IEfremov\ModuleDesigner\Actions\CallModuleComponentCommandAction;

class ActionServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     * 
     * @var array<string, string>
     */
    public $bindings = [
        GetModuleComponentsOptions::class => GetModuleComponentsOptionsAction::class,
        CreateModuleRequestRule::class => CreateModuleRequestRuleAction::class,
        FilterCreateRequest::class => FilterCreateRequestAction::class,
        CallModuleComponentCommand::class => CallModuleComponentCommandAction::class
    ];
}