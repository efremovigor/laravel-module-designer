<?php

namespace IEfremov\ModuleDesigner\Providers;

use Illuminate\Support\ServiceProvider;
use IEfremov\ModuleDesigner\Commands\ControllerMakeCommand;
use IEfremov\ModuleDesigner\Commands\ModelMakeCommand;
use IEfremov\ModuleDesigner\Commands\RequestMakeCommand;
use IEfremov\ModuleDesigner\Commands\FactoryMakeCommand;
use IEfremov\ModuleDesigner\Commands\MigrationMakeCommand;
use IEfremov\ModuleDesigner\Commands\SeederMakeCommand;
use IEfremov\ModuleDesigner\Commands\PolicyMakeCommand;
use IEfremov\ModuleDesigner\Commands\MiddlewareMakeCommand;
use IEfremov\ModuleDesigner\Commands\ProviderMakeCommand;

class ModuleDesignerServiceProvider extends ServiceProvider
{

    /**
     * The commands to be registered.
     *
     * @var array<string, string>
     */
    protected $commands = [
        'ControllerMake' => ControllerMakeCommand::class,
        'ModelMake' => ModelMakeCommand::class,
        'RequestMake' => RequestMakeCommand::class,
        'FactoryMake' => FactoryMakeCommand::class,
        'MigrationMake' => MigrationMakeCommand::class,
        'SeederMake' => SeederMakeCommand::class,
        'PolicyMake' => PolicyMakeCommand::class,
        'MiddlewareMake' => MiddlewareMakeCommand::class,
        'ProviderMake' => ProviderMakeCommand::class
    ];

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'module-designer');

        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/module-designer'),
        ], 'module-designer');

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCommands($this->commands);

        $this->mergeConfigFrom(__DIR__ . '/../config/module_components.php', 'module_components');

        $this->mergeConfigFrom(__DIR__ . '/../config/module_config.php', 'module_config');

    }


    /**
     * Register the given commands.
     *
     * @param  array<string, string>  $commands
     * @return void
     */
    protected function registerCommands(array $commands)
    {
        foreach ($commands as $commandName => $command) {
            $method = "register{$commandName}Command";

            if (method_exists($this, $method)) {
                $this->{$method}();
            } else {
                $this->app->singleton($command);
            }
        }

        $this->commands(array_values($commands));
    }

    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerControllerMakeCommand()
    {
        $this->app->singleton(ControllerMakeCommand::class, function ($app) {
            return new ControllerMakeCommand($app['files']);
        });
    }

    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerModelMakeCommand()
    {
        $this->app->singleton(ModelMakeCommand::class, function ($app) {
            return new ModelMakeCommand($app['files']);
        });
    }

    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerRequestMakeCommand()
    {
        $this->app->singleton(RequestMakeCommand::class, function ($app) {
            return new RequestMakeCommand($app['files']);
        });
    }

    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerFactoryMakeCommand()
    {
        $this->app->singleton(FactoryMakeCommand::class, function ($app) {
            return new FactoryMakeCommand($app['files']);
        });
    }

    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerMigrationMakeCommand()
    {
        $this->app->singleton(MigrationMakeCommand::class, function ($app) {
            
            $creator = $app['migration.creator'];

            $composer = $app['composer'];

            return new MigrationMakeCommand($creator, $composer);
        });
    }

    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerSeederMakeCommand()
    {
        $this->app->singleton(SeederMakeCommand::class, function ($app) {
            return new SeederMakeCommand($app['files']);
        });
    }

    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerPolicyMakeCommand()
    {
        $this->app->singleton(PolicyMakeCommand::class, function ($app) {
            return new PolicyMakeCommand($app['files']);
        });
    }

    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerMiddlewareMakeCommand()
    {
        $this->app->singleton(MiddlewareMakeCommand::class, function ($app) {
            return new MiddlewareMakeCommand($app['files']);
        });
    }

    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerProviderMakeCommand()
    {
        $this->app->singleton(ProviderMakeCommand::class, function ($app) {
            return new ProviderMakeCommand($app['files']);
        });
    }

}