<?php

namespace IEfremov\ModuleDesigner\Contracts\Actions;

interface FilterCreateRequest
{
    /**
     * @param  \IEfremov\ModuleDesigner\Http\Requests\CreateModuleRequest|null  $request
     * 
     * @return array<string, array<string, array<string>>>
     */
    public function __invoke($request = null);
}