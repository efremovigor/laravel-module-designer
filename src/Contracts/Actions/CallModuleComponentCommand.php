<?php

namespace IEfremov\ModuleDesigner\Contracts\Actions;

interface CallModuleComponentCommand
{
    /**
     * @param  array<string, array<string, string|array<int|string, string|null>>>  $request
     * 
     * @return array<string, int>
     */
    public function __invoke($request = []);
}