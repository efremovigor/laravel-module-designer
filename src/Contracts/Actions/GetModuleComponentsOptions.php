<?php

namespace IEfremov\ModuleDesigner\Contracts\Actions;

interface GetModuleComponentsOptions
{
    /**
     * @return array<string, array<string, array<int, array<int, int|string|null>>>>
     */
    public function __invoke();
}