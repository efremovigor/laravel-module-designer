<?php

namespace IEfremov\ModuleDesigner\Contracts\Actions;

interface CreateModuleRequestRule
{
    /**
     * @return array<string, string|array<int, string>>
     */
    public function __invoke();
}