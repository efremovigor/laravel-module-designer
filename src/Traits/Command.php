<?php

namespace IEfremov\ModuleDesigner\Traits;

use Illuminate\Support\Str;

trait Command 
{
    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace()
    {
        return config('module_config.namespace') . '\\' . config('module_config.name') . '\\';
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $rootPath = config('module_config.path');

        return base_path() . '/' . $rootPath . '/' .str_replace('\\', '/', $name).'.php';
    }

    /**
     * Get the console command options.
     *
     * @return array<int, array<int, int|string|null>>
     */
    public function getOptionsList()
    {
        return $this->getOptions();
    }

    /**
     * Qualify the given model class base name.
     *
     * @param  string  $model
     * @return string
     */
    protected function qualifyModel(string $model)
    {
        $model = ltrim($model, '\\/');

        $model = str_replace('/', '\\', $model);

        $rootNamespace = $this->rootNamespace();

        if (Str::startsWith($model, $rootNamespace)) {
            return $model;
        }

        return $rootNamespace.'Models\\'.$model;
    }
}