<?php

namespace IEfremov\ModuleDesigner\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use IEfremov\ModuleDesigner\Contracts\Actions\CreateModuleRequestRule;

class CreateModuleRequest extends FormRequest
{

    /**
     * @param  \IEfremov\ModuleDesigner\Actions\CreateModuleRequestRuleAction  $createModuleRequestRule
     *
     * @return array<string, string|array<int, string>>
     */
    public function rules(CreateModuleRequestRule $createModuleRequestRule): array
    {
        $rules = $createModuleRequestRule();
        $rules['config'] = 'required';

        return $rules;
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array<string, string>
     */
    public function messages()
    {
        return [
            'config.required' => 'You have not selected any options!'
        ];
    }
}
