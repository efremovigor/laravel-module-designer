<?php

namespace IEfremov\ModuleDesigner\Http\Controllers;

use Illuminate\Routing\Controller;
use IEfremov\ModuleDesigner\Contracts\Actions\GetModuleComponentsOptions;
use Illuminate\View\View;
use IEfremov\ModuleDesigner\Actions\Group\ProcessCreateModuleRequestAction;
use IEfremov\ModuleDesigner\Contracts\Actions\FilterCreateRequest;
use IEfremov\ModuleDesigner\Contracts\Actions\CallModuleComponentCommand;

class ModuleDesignerController extends Controller
{
    /**
     * @param  \IEfremov\ModuleDesigner\Contracts\Actions\GetModuleComponentsOptions  $moduleComponentsOptions
     * 
     * @return \Illuminate\View\View
     */
    public function create(GetModuleComponentsOptions $moduleComponentsOptions): View
    {
        $result = $moduleComponentsOptions();
        $options = json_encode($result['options']);
        $input = json_encode($result['input']);
        $config = json_encode(config('module_config'));

        return view('module-designer::create', compact('options', 'input', 'config'));
    }

    /**
     * @param  \IEfremov\ModuleDesigner\Actions\Group\ProcessCreateModuleRequestAction  $processCreateModuleRequestAction
     * @param  \IEfremov\ModuleDesigner\Contracts\Actions\FilterCreateRequest  $filterCreateRequest
     * @param  \IEfremov\ModuleDesigner\Contracts\Actions\CallModuleComponentCommand  $callModuleComponentCommand
     * 
     * @return string|false
     */
    public function process(
        ProcessCreateModuleRequestAction $processCreateModuleRequestAction,
        FilterCreateRequest $filterCreateRequest,
        CallModuleComponentCommand $callModuleComponentCommand,
    )
    {
        $result = $processCreateModuleRequestAction(
            $filterCreateRequest,
            $callModuleComponentCommand,
        );

        return json_encode($result);
    }
}