import './bootstrap'
import {createApp} from 'vue'
import ModuleCreateForm from './components/ModuleCreateForm.vue'

const app = createApp({})

app.component('module-create-form', ModuleCreateForm)

app.mount("#app")