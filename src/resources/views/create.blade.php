@extends('module-designer::layouts.module-designer')

@section('title', 'Module designer')

@section('content')
    <module-create-form 
        :options="{{ $options }}" 
        :input="{{ $input }}" 
        :config="{{ $config }}">
    </module-create-form>
@endsection

