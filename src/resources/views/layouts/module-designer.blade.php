<!doctype html>

<html>
    
    @include('module-designer::include.head')

    <body>
        <div class="container">
            
            <h1>@yield('title')</h1>
            
            <div id="app">
            
                @yield('content')
                
            </div>
        </div>
        <script src="{{ Vite::asset('resources/js/module-designer.js', 'vendor/module-designer/build') }}"></script>
        
        
    </body>

</html>