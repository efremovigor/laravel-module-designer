<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>
    
    <link href="{{ Vite::asset('resources/sass/module-designer.scss', 'vendor/module-designer/build') }}" rel="stylesheet">
    
</head>
