<?php

namespace IEfremov\ModuleDesigner\Actions;

use IEfremov\ModuleDesigner\Contracts\Actions\FilterCreateRequest;

class FilterCreateRequestAction implements FilterCreateRequest
{
    /**
     * @param  \IEfremov\ModuleDesigner\Http\Requests\CreateModuleRequest|null  $request
     * 
     * @return array<string, array<string, array<string>>>
     */
    public function __invoke($request = null)
    {
        if(empty($request)) {
            return [];
        }
        
        /** @var array<string, string> $moduleComponents */
        $moduleComponents = config('module_components');
        $moduleComponents[] = 'config';
        
        $requestFilter = $request->only($moduleComponents);
        
        return $requestFilter;

    }
}