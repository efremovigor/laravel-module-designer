<?php

namespace IEfremov\ModuleDesigner\Actions\Group;

use IEfremov\ModuleDesigner\Http\Requests\CreateModuleRequest;
use IEfremov\ModuleDesigner\Contracts\Actions\FilterCreateRequest;
use IEfremov\ModuleDesigner\Contracts\Actions\CallModuleComponentCommand;

class ProcessCreateModuleRequestAction
{

    /**
     * @var \IEfremov\ModuleDesigner\Http\Requests\CreateModuleRequest
     */
    private $request;

    /**
     * @param  \IEfremov\ModuleDesigner\Http\Requests\CreateModuleRequest  $request
     */
    public function __construct(
        CreateModuleRequest $request
    )
    {
        $this->request = $request;
    }

    /**
     * @return array<string, int>
     */
    public function __invoke(
        FilterCreateRequest $filterCreateRequest,
        CallModuleComponentCommand $callModuleComponentCommand
    )
    {
        $request = $filterCreateRequest($this->request);
        $result = $callModuleComponentCommand($request);

        return $result;
    }
}