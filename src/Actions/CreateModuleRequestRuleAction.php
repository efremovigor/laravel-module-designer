<?php

namespace IEfremov\ModuleDesigner\Actions;

use IEfremov\ModuleDesigner\Contracts\Actions\CreateModuleRequestRule;
use IEfremov\ModuleDesigner\Contracts\Actions\GetModuleComponentsOptions;

class CreateModuleRequestRuleAction implements CreateModuleRequestRule
{
    /**
     * @return array<string, string|array<int, string>>
     */
    public function __invoke()
    {
        $moduleComponentsOptions = app(GetModuleComponentsOptions::class);

        $result = $moduleComponentsOptions();

        $options = $result['options'];
        
        $rules = [];
        foreach(array_keys($options) as $key) {
            $rules[$key . '.values'] = 'array';
            $rules[$key . '.options'] = 'array';
        }

        /** @var array<string, string> $moduleConfig */
        $moduleConfig = config('module_config');
        
        foreach(array_keys($moduleConfig) as $key) {
            $rules['config.'.$key] = ['alpha:ascii', 'nullable'];
        }

        return $rules;
    }
}