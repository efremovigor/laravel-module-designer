<?php

namespace IEfremov\ModuleDesigner\Actions;

use IEfremov\ModuleDesigner\Contracts\Actions\CallModuleComponentCommand;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Artisan;

class CallModuleComponentCommandAction implements CallModuleComponentCommand
{
    CONST COMMAND_PREFIX = 'efremov:make_';

    /**
     * @param  array<string, array<string, string|array<int|string, string|null>>>  $request
     * 
     * @return array<string, int>
     */
    public function __invoke($request = [])
    {
        foreach($request['config'] as $key => $config) {
            if(!empty($config)) {
                Config::set("module_config.{$key}", $config);
            }
        }

        unset($request['config']);

        $result = [];
        foreach($request as $key => $items) {

            $params = [];
            $params['name'] = empty($request[$key]['name']) ? 'Custom' . ucfirst($key) : $request[$key]['name'];

            if(!empty($items['values']) && is_array($items['values'])) {
                foreach($items['values'] as $value) {
                    if(!empty($items['options'][$value])) {
                        $params['--' . $value] = $items['options'][$value];
                    } else {
                        $params['--' . $value] = true;
                    }
                }
            }

            $exitcode = Artisan::call(self::COMMAND_PREFIX . $key, $params);

            $result[$key] = $exitcode;
            
        }

        return $result;
    }
}