<?php

namespace IEfremov\ModuleDesigner\Actions;

use IEfremov\ModuleDesigner\Contracts\Actions\GetModuleComponentsOptions;
use Symfony\Component\Console\Input\InputOption;

class GetModuleComponentsOptionsAction implements GetModuleComponentsOptions
{
    /**
     * @return array<string, array<string, array<int, array<int, int|string|null>>>>
     */
    public function __invoke()
    {
        $commandsNameSpace = 'IEfremov\ModuleDesigner\Commands';

        /** @var array<int, string> $components */
        $components = config('module_components');
        $result = [];
        $options = [];
        foreach($components as $component) {
            $class = $commandsNameSpace . "\\" . ucfirst($component) . 'MakeCommand';
            if(class_exists($class)) {
                $options[$component] = app($class)->getOptionsList();
            }
        }

        // Find options requiring additional data input
        $required = [InputOption::VALUE_REQUIRED, InputOption::VALUE_OPTIONAL];
        $requiredValues = [];
        foreach($options as $key => $items) {
            $requiredValues[$key] = ['options' => []];
            foreach($items as $values) {
                if(in_array($values[2], $required)) {
                    $requiredValues[$key]['options'][$values[0]] = '';
                }
            }
        }

        $result['options'] = $options;
        $result['input'] = $requiredValues;

        return $result;
    }
}