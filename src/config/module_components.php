<?php

// List of module components

return [
    'controller',
    'factory',
    'middleware',
    'migration',
    'model',
    'policy',
    'provider',
    'request',
    'seeder',
    'view'
];