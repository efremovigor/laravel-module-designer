<?php

namespace IEfremov\ModuleDesigner\Tests\Feature;

use Tests\TestCase;
use IEfremov\ModuleDesigner\Contracts\Actions\GetModuleComponentsOptions;

class GetModuleComponentsOptionsTest extends TestCase
{

    public function testGetModuleComponentsOptionsAction(): void
    {
        $action = app(GetModuleComponentsOptions::class);

        $result = $action();

        $this->assertArrayHasKey('options', $result);

        $this->assertArrayHasKey('input', $result);

        $this->assertCount(2, $result);
    }
}
