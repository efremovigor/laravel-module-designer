<?php

namespace IEfremov\ModuleDesigner\Tests\Feature;

use Tests\TestCase;
use IEfremov\ModuleDesigner\Contracts\Actions\CreateModuleRequestRule;

class CreateModuleRequestRuleTest extends TestCase
{

    public function testCreateModuleRequestRuleAction(): void
    {
        $action = app(CreateModuleRequestRule::class);

        $result = $action();

        $this->assertArrayHasKey('controller.values', $result);

        $this->assertArrayHasKey('config.path', $result);
    }
}
