<?php

namespace IEfremov\ModuleDesigner\Tests\Feature;

use Tests\TestCase;
use IEfremov\ModuleDesigner\Contracts\Actions\CallModuleComponentCommand;

class CallModuleComponentCommandActionTest extends TestCase
{
    public function testControllerCreateAction(): void
    {
        $action = app(CallModuleComponentCommand::class);

        /** @var array<string, string> $config */
        $config = config('module_config');
        $data = [
            'config' => $config,
            'controller' => [
                'name' => 'TestController',
                'values' => ['api'],
                'options' => []
            ]
        ];

        $result = $action($data);

        $this->assertArrayHasKey('controller', $result);

        $this->assertEquals(0, $result['controller']);

    }
}