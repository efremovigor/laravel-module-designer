<?php

namespace IEfremov\ModuleDesigner\Tests\Feature;

use Tests\TestCase;

class ModuleDesignerCreateTest extends TestCase
{

    public function testResponseStatus(): void
    {
        $createRoute = route('module-designer.create');

        $response = $this->get($createRoute);

        $response->assertStatus(200);
    }

    public function testViewRendering(): void
    {
        $options = json_encode([]);
        $input = json_encode([]);
        $config = json_encode(config('module_config'));
        $view = $this->view('module-designer::create', compact('options', 'input', 'config'));

        $view->assertSee('Module designer');
    }
}
